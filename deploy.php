<?php
namespace Deployer;

require 'recipe/common.php';

// Configuration
set('repository', 'git@framagit.org:nicosomb/planet-techblog.git');
set('git_tty', true); // [Optional] Allocate tty for git on first deployment
set('shared_files', [
    'custom/config.yml',
    '/custom/people.opml',
    '/custom/people.opml.bak',
    '/admin/inc/pwd.inc.php',
]);
set('shared_dirs', [
    'cache',
]);
set('writable_dirs', ['cache']);
set('allow_anonymous_stats', false);
set('keep_releases', 3);

// Hosts
host('ghinzu')
    ->stage('production')
    ->set('deploy_path', '/data/www/planet-techblog.com/www')
    ->user('deploy')
    ->port(22)
    ->identityFile('~/.ssh/id_rsa');

// Tasks
desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('sudo /etc/init.d/nginx reload');
});
after('deploy:symlink', 'php-fpm:restart');

desc('Deploy your project');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
